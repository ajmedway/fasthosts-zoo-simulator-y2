<?php declare(strict_types=1);

namespace app\controllers;

use app\models\Zoo;
use Yii;

/**
 * Class ZooController
 * @package app\controllers
 */
class ZooController extends \yii\web\Controller
{
    public object $session;
    public object $zoo;

    /**
     * ZooController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        // pass constructor params onwards up the inheritance chain
        parent::__construct($id, $module, $config);

        $this->session = Yii::$app->session;

        // might have been better to create the animal objects from config files, e.g. json, or load from database?
        if (!$this->session->has('zoo')) {
            $this->zoo = new Zoo();
            $this->session->set('zoo', $this->zoo);
        } else {
            $this->zoo = $this->session->get('zoo');
        }
    }

    /**
     * Show the welcome page.
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Feed the animals.
     * @return string
     */
    public function actionFeed()
    {
        // increment time
        $this->zoo->incrementTime();

        // run feed method of Animal
        $this->zoo->feedAnimals();

        // NOTE: It wasn't clear from the brief if the feed run should also come with a call to the health method to
        // reduce the health of the Animals. My interpretation was that they were meant to be separate and mutually
        // exclusive. If that interpretation was incorrect, the following line is to be uncommented:
        #$this->zoo->updateAnimals();

        // update the Zoo object in session
        $this->session->set('zoo', $this->zoo);

        // pass the Zoo object into the view and render
        return $this->render('feed', [
            'zoo' => $this->zoo,
        ]);
    }

    /**
     * Walk around the zoo.
     * @return string
     */
    public function actionProceed()
    {
        // increment time
        $this->zoo->incrementTime();

        // run health method of Animal only after the first iteration to zero
        if ($this->zoo->getTime() > 0) {
            $this->zoo->updateAnimals();
        }

        // update the Zoo object in session
        $this->session->set('zoo', $this->zoo);

        // pass the Zoo object into the view and render
        return $this->render('proceed', [
            'zoo' => $this->zoo,
        ]);
    }

    /**
     * Clean up the zoo, replenish the animals.
     * @return string
     */
    public function actionReset()
    {
        // check if a session is already open
        if ($this->session->isActive) {
            $this->session->destroy();
        }

        // open a new session
        $this->session->open();

        return $this->render('reset');
    }
}
