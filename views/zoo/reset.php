<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
?>
<div class="zoo-reset">

    <div class="jumbotron">
        <h1><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Zoo Reset</h1>

        <p class="lead">The Zoo has been cleaned and the animals have been replenished.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['zoo/proceed']) ?>">Go see the animals!</a></p>
    </div>

</div>
