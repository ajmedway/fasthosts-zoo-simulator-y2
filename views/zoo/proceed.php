<?php
/* @var $this yii\web\View */
/* @var $zoo app\models\Zoo */
?>
<h1><span class="glyphicon glyphicon-time" aria-hidden="true"></span> zoo/proceed</h1>

<?= $this->render('_zoo-stats', [
    'zoo' => $zoo
]) ?>
