<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = Yii::$app->name;
?>
<div class="zoo-index">

    <div class="jumbotron">
        <h1><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Welcome!</h1>

        <p class="lead">You have arrived at
            <strong><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> Alec Pritchard's
                Zoo</strong> simulation test for Fasthosts.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['zoo/proceed']) ?>">Go see the animals!</a></p>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Zoo Simulator Test Spec</h2>
            <p>
                Write a simple Zoo simulator which contains 3 different types of animal: monkey,
                giraffe and elephant. The zoo should open with 5 of each type of animal.
            </p>
            <p>
                Each animal has a health value held as a percentage (100% is completely healthy).
                Every animal starts at 100% health. This value should be a floating point value.
                The application should act as a simulator, with time passing at the rate of 1 hour
                with each iteration. Every hour that passes, a random value between 0 and 20 is to be
                generated for each animal. This value should be passed to the appropriate animal, whose
                health is then reduced by that percentage of their current health.
            </p>
            <p>
                The user must be able to feed the animals in the zoo. When this happens, the zoo
                should generate three random values between 10 and 25; one for each type of animal. The
                health of the respective animals is to be increased by the specified percentage of their
                current health. Health should be capped at 100%.
            </p>
            <p>
                When an Elephant has a health below 70% it cannot walk. If its health does not
                return above 70% once the subsequent hour has elapsed, it is pronounced dead.
                When a Monkey has a health below 30%, or a Giraffe below 50%, it is pronounced
                dead straight away.
            </p>
            <p>
                The user interface should show the current status of each Animal and contain two
                buttons, one to provoke an hour of time to pass and another to feed the zoo. The UI should
                update to reflect each change in state, and the current time at the zoo.
            </p>
        </div>
    </div>

</div>
