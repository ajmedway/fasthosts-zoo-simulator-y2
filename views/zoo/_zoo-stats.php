<?php
/* @var $this yii\web\View */
/* @var $zoo app\models\Zoo */

use app\models\AbstractAnimal;
use app\models\Elephant;
use app\models\Lion;
use yii\helpers\Inflector;

$session = \Yii::$app->session;
?>
<h3>Total hours elapsed: <?= $zoo->getTime("%02d:00") ?></h3>

<div class="row">
    <?php foreach ($zoo->getAnimals() as $animalType => $animals): ?>
        <div class="col-md-3">
            <h2><?= Inflector::pluralize($animalType) ?></h2>
            <dl>
                <?php foreach ($animals as $animal): ?>
                    <?php if ($animal instanceof AbstractAnimal): ?>
                        <dt><h4><?= $animal->name ?></h4></dt>
                        <dd>
                            <span>Health <span class="label label-primary"><?= number_format($animal->health, 2) ?>%</span></span>
                            <br>
                            <span>Status
                                <?php if ($animal->alive): ?>
                                    <span class="label label-success"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> Alive</span>
                                <?php else: ?>
                                    <span class="label label-danger"><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> Dead</span>
                                <?php endif; ?>
                                <?php if ($animal::$type === Elephant::$type && $animal->alive && $animal->criticalHealth): ?>
                                    <span class="label label-warning"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Critical!</span>
                                <?php endif; ?>
                                <?php if ($animal::$type === Lion::$type && $animal->alive && $animal->eatenAnimalOnLastIteration): ?>
                                    <span class="label label-primary"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Predator WIN!</span>
                                <?php endif; ?>
                            </span>
                        </dd>
                    <?php endif; ?>
                <?php endforeach; ?>
            </dl>
        </div>
    <?php endforeach; ?>
</div>
