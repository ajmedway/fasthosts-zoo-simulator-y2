<?php declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\BaseObject;
use yii\base\ErrorException;

/**
 * Class AbstractAnimal
 * @package app\models
 */
abstract class AbstractAnimal extends BaseObject
{
    public static string $type;

    public int $id;
    public string $name;
    public bool $alive = true;
    public float $health = 100.00;

    /**
     * AbstractAnimal constructor.
     * @param int $id
     * @param string $name
     * @param array $config
     */
    public function __construct(int $id, string $name, array $config = [])
    {
        // initialise properties
        $this->id = $id;
        $this->name = $name;

        // call parent implementation
        parent::__construct($config);
    }

    /**
     * Increase current health value by given percentage.
     * @param float $percentage health change percentage
     */
    public function upgradeHealth(float $percentage): void
    {
        $this->modifyHealth($percentage, '+');
    }

    /**
     * Modify current health value by given percentage and operator.
     * @param float $percentage health change percentage
     * @param string $operator operator to use to calculate new health value
     */
    protected function modifyHealth(float $percentage, string $operator): void
    {
        // stop processing health modifications after the animal has died
        if (!$this->alive) {
            return;
        }
        // only allow whitelisted operators
        if (in_array($operator, ['+', '-'])) {
            // calculate the percentage change on current health
            $change = (float)$this->health * ($percentage / 100);
            // modify to the health value
            switch ($operator) {
                case '+':
                    $this->health += $change;
                    break;
                case '-':
                    $this->health -= $change;
                    break;
            }
            // cap the health value at 100
            if ($this->health > 100) $this->health = 100;
            // prevent modification to a negative health value
            if ($this->health < 0) $this->health = 0;
            // check health/manage alive status accordingly
            $this->checkHealth();
        } else {
            Yii::$app->session->setFlash('warning', "Health could not be updated for animal #{$this->id}");
            // log error message
            Yii::error("Health could not be updated for animal #{$this->id} - Invalid operator used.");
        }
    }

    /**
     * Abstract method for checking health/managing `alive` status accordingly.
     * @return bool
     */
    abstract function checkHealth(): bool;

    /**
     * Decrease current health value by given percentage.
     * @param float $percentage health change percentage
     */
    public function downgradeHealth(float $percentage): void
    {
        $this->modifyHealth($percentage, '-');
    }

    /**
     * One-way ticket to death.
     */
    public function pronounceDead(): void
    {
        $this->alive = false;
    }
}
