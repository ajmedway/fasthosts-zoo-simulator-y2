<?php declare(strict_types=1);

namespace app\models;

use yii\base\Exception;

/**
 * Class Zoo
 * @package app\models
 */
class Zoo implements ZooInterface
{
    /**
     * @var int Total number of animals of each kind to create.
     */
    protected static int $numberOfAnimals = 5;
    /**
     * @var int Min health loss per iteration.
     */
    protected static int $minHealthLoss = 0;
    /**
     * @var int Max health loss per iteration.
     */
    protected static int $maxHealthLoss = 20;
    /**
     * @var int Min health gain per iteration.
     */
    protected static int $minHealthGain = 10;
    /**
     * @var int Max health gain per iteration.
     */
    protected static int $maxHealthGain = 25;
    /**
     * @var array Array of animal types, each with a child array of AbstractAnimal instances.
     */
    protected array $animals = [];
    /**
     * @var int The total number of hours elapsed since the Zoo was created or reset.
     */
    protected int $timeHours = -1;

    /**
     * @var int How many animals got eaten by lion.
     */
    protected int $killedByLions = 0;

    /**
     * Zoo constructor.
     */
    public function __construct()
    {
        $this->createAnimals();
    }

    /**
     * Create the animals, with `self::$numberOfAnimals` of each kind.
     * @return $this
     */
    public function createAnimals(): self
    {
        $faker = \Faker\Factory::create();
        $animalTypes = [
            Elephant::$type,
            Giraffe::$type,
            Lion::$type,
            Monkey::$type,
        ];
        $animals = [];
        $count = 0;
        foreach ($animalTypes as $animalType) {
            for ($i = 1; $i <= self::$numberOfAnimals; $i++) {
                $count++;
                try {
                    $animals[$animalType][] = AnimalFactory::create($animalType, $count, $faker->firstName);
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', $e->getMessage());
                    // log error message
                    Yii::error($e->getMessage());
                }
            }
        }
        $this->animals = $animals;
        return $this;
    }

    /**
     * Get the time from the tracker.
     * @param string|null $sprintfFormat
     * @return string
     */
    public function getTime(?string $sprintfFormat = null): string
    {
        if (!empty($sprintfFormat)) {
            return sprintf($sprintfFormat, $this->timeHours);
        }
        return (string)$this->timeHours;
    }

    /**
     * Add an hour to the time tracker.
     */
    public function incrementTime(): void
    {
        $this->timeHours++;
    }

    /**
     * Get the loaded animals array.
     * @return array
     */
    public function getAnimals(): array
    {
        return $this->animals;
    }

    /**
     * Update the animals with standard iteration, downgrading health by a randomised percentage value per animal type.
     * @return $this
     */
    public function updateAnimals(): self
    {
        foreach ($this->animals as $animalType => $animals) {
            foreach ($animals as $animal) {
                if ($animal instanceof AbstractAnimal) {
                    $randPercentage = self::getRandomNumber(self::$minHealthLoss, self::$maxHealthLoss);
                    $animal->downgradeHealth(
                        $randPercentage
                    );
                }
            }
        }
        return $this;
    }

    /**
     * Generate random number within bounds.
     * @param int $min
     * @param int $max
     * @return int
     */
    private static function getRandomNumber(int $min = 0, int $max = 100): int
    {
        return rand($min, $max);
    }

    /**
     * Update the animals with feed iteration, upgrading health by a randomised percentage value per animal type.
     * @return $this
     */
    public function feedAnimals(): self
    {
        foreach ($this->animals as $animalType => $animals) {
            $randPercentage = self::getRandomNumber(self::$minHealthGain, self::$maxHealthGain);
            foreach ($animals as $animal) {
                if ($animal instanceof AbstractAnimal) {
                    // if this is a Lion, check if escaped?!
                    if ($animal instanceof Lion && $animal->escaped()) {
                        $animal->escape();
                        $this->killedByLions++;
                        continue;
                    }
                    $animal->upgradeHealth(
                        $randPercentage
                    );
                }
            }
        }

        // kill any animals that have been eaten
        $totalKilledOff = $this->killTheAnimalsEatenByLions();

        return $this;
    }

    /**
     * @return int The total number eaten/killed
     */
    public function killTheAnimalsEatenByLions(): int
    {
        $count = 0;
        if ($this->killedByLions > 0) {
            foreach ($this->animals as $animalType => $animals) {
                foreach ($animals as $animal) {
                    if ($animal instanceof AbstractAnimal && !($animal instanceof Lion)) {
                        $animal->pronounceDead();
                        $count++;
                        if ($this->killedByLions <= $count) {
                            break 2;
                        }
                    }
                }
            }
        }
        return $count;
    }
}
