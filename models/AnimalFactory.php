<?php declare(strict_types=1);

namespace app\models;

use yii\base\Exception;

/**
 * Class AnimalFactory, to create Animals using simple factory pattern.
 * @package app\models
 */
class AnimalFactory
{
    /**
     * @param string $type Animal type as defined in concrete classes extending from AbstractAnimal
     * @param int $id Id for animal instance.
     * @param string $name Personal name for the animal.
     * @return AbstractAnimal
     * @throws Exception
     */
    public static function create(string $type, int $id, string $name): AbstractAnimal
    {
        if (empty($id)) {
            throw new Exception('Cannot create animal. Missing id value.');
        }
        if (empty($name)) {
            throw new Exception('Cannot create animal. Missing name value.');
        }
        switch ($type) {
            case Elephant::$type:
                return new Elephant($id, $name);
            case Giraffe::$type:
                return new Giraffe($id, $name);
            case Lion::$type:
                return new Lion($id, $name);
            case Monkey::$type:
                return new Monkey($id, $name);
            default:
                throw new Exception("Cannot create animal. Invalid animal type <em>{$type}</em> passed.");
        }
    }
}
