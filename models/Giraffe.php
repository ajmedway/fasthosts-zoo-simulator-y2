<?php declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\ErrorException;

/**
 * Class Giraffe
 * @package app\models
 */
class Giraffe extends AbstractAnimal
{
    public static string $type = 'Giraffe';
    public int $lifeThreshold = 50;

    /**
     * Check health/manage `alive` status accordingly.
     * @return bool if the animal is alive
     */
    function checkHealth(): bool
    {
        if ($this->health <= $this->lifeThreshold) {
            $this->pronounceDead();
        }
        return $this->alive;
    }
}
