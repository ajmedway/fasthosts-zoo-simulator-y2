<?php declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\ErrorException;

/**
 * Class Lion
 * @package app\models
 */
class Lion extends AbstractAnimal
{
    public static string $type = 'Lion';
    public int $lifeThreshold = 50;
    public bool $eatenAnimalOnLastIteration = false;

    /**
     * Check health/manage `alive` status accordingly.
     * @return bool if the animal is alive
     */
    function checkHealth(): bool
    {
        if ($this->health <= $this->lifeThreshold) {
            $this->pronounceDead();
        }
        $this->eatenAnimalOnLastIteration = false;
        return $this->alive;
    }

    /**
     * @param int $randMin
     * @param int $randMax
     * @return bool Did the Lion escape?
     */
    function getEscapeStatus(int $randMin = 0, int $randMax = 1): bool
    {
        return (bool)rand($randMin, $randMax);
    }

    /**
     * Escaped! Set health to 100, mark as having eaten another animal on last iteration.
     */
    function escaped(): void
    {
        $this->eatenAnimalOnLastIteration = true;
        $this->health = 100.00;
    }
}
