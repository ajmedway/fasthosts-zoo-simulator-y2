<?php declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\ErrorException;

/**
 * Class Elephant
 * @package app\models
 */
class Elephant extends AbstractAnimal
{
    public static string $type = 'Elephant';
    public int $walkThreshold = 70;
    public bool $criticalHealth = false;

    /**
     * Check health/manage `alive` status accordingly.
     * @return bool if the elephant is alive
     */
    function checkHealth(): bool
    {
        if ($this->health <= $this->walkThreshold && $this->criticalHealth === true) {
            // if the elephant in critical condition already from last iteration, pronounce dead
            $this->pronounceDead();
        } elseif ($this->health <= $this->walkThreshold) {
            // mark the elephant as being in critical condition, unable to walk
            $this->criticalHealth = true;
        } else {
            // mark the elephant as not being in critical condition, able to walk
            $this->criticalHealth = false;
        }
        return $this->alive;
    }
}
