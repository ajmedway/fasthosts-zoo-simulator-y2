<?php declare(strict_types=1);

namespace app\models;

interface ZooInterface
{
    public function __construct();

    public function getAnimals(): array;

    public function createAnimals(): self;

    public function updateAnimals(): self;

    public function feedAnimals(): self;
}
