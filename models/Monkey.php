<?php declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\ErrorException;

/**
 * Class Monkey
 * @package app\models
 */
class Monkey extends AbstractAnimal
{
    public static string $type = 'Monkey';
    public int $lifeThreshold = 30;

    /**
     * Check health/manage `alive` status accordingly.
     * @return bool if the animal is alive
     */
    function checkHealth(): bool
    {
        if ($this->health <= $this->lifeThreshold) {
            $this->pronounceDead();
        }
        return $this->alive;
    }
}
